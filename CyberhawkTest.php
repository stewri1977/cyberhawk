<?php

class CyberhawkTest
{

    private $three = "Coating Damage";
    private $five = "Lightning Strike";
    private $fifteen = "";
    private $output = [];


    public function __construct()
    {
        $this->fifteen = $this->three . " and " . $this->five;
    }

    /**
     * @param $pageInfo
     */
    public function getPagingData($pageInfo)
    {

        $size = $pageInfo['size'];
        $number = $pageInfo['number'];

        $min = ($number - 1) * $size;
        $max = ($number * $size) - 1;

        $this->calculateOutput($min, $max);
    }

    /**
     * @param int $min
     * @param int $max
     */
    public function calculateOutput(int $min = 0, int $max = 99)
    {
        for ($i = $min; $i <= $max; $i++) {
            if ($this->findFifteen($i)) {
                $this->output[] = ["value" => $this->fifteen, "class" => "fifteen"];
            } elseif ($this->findThrees($i)) {
                $this->output[] = ["value" => $this->three, "class" => "three"];
            } elseif ($this->findFives($i)) {
                $this->output[] = ["value" => $this->five, "class" => "five"];
            } else {
                $this->output[] = ["value" => $i, "class" => "normal"];
            }
        }
    }


    public function getFives()
    {
        $this->output = array_keys(array_column($this->output, "value"), $this->five, true);
    }

    public function getThrees()
    {
        $this->output = array_keys(array_column($this->output, "value"), $this->three, true);
    }

    public function getThreesAndFives()
    {
        $this->output = array_keys(array_column($this->output, "value"), $this->fifteen, true);
    }

    public function formatPageData()
    {
        $this->output = array_column($this->output, "value");
    }

    /**
     * @param  $value
     * @return bool
     */
    private function findThrees($value)
    {
        if ($value != 0 && $value % 3 == 0) {
            return true;
        }
        return false;
    }

    /**
     * @param  $value
     * @return bool|string
     */
    private function findFives($value)
    {
        if ($value != 0 && $value % 5 == 0) {
            return true;
        }
        return false;
    }

    /**
     * @param  $value
     * @return bool|string
     */
    private function findFifteen($value)
    {
        if ($value != 0 && $value % 15 == 0) {
            return true;
        }
        return false;
    }

    /**
     * @return array
     */
    public function getOutput()
    {
        return $this->output;
    }
}
