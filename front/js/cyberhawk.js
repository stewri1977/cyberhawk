var cyberhawk = {
    element:false,
    backUrl:"http://cyber-env.gtum6xbrmd.eu-west-2.elasticbeanstalk.com/cyberhawk/",
    mainSuccess: function (data, outputClass, useValue) {
        $("."+outputClass).empty();
        var appendValueClass = "class='normal'";
        $.each(
            data,
            function ( key, outValue ) {
                if (useValue) {
                    appendValueClass = "class="+"'"+outValue.class+"'";
                    outValue = outValue.value;
                }

                $("."+outputClass).append("<span "+appendValueClass+">"+outValue+"</span>")
            }
        );
    },
    ajaxRequest: function (outputClass, useValue = true, action="") {
        $.ajax(
            {
                url: cyberhawk.backUrl+"?action="+action,
                type: "GET",
                data: {},
                dataType: 'json',
                success: function (data) {
                    cyberhawk.mainSuccess(data, outputClass, useValue);
                }}
        );
    }
};


$(document).ready(
    function () {
        $('.mainAction').click(
            function () {
                cyberhawk.ajaxRequest("result");
            }
        );

        $('.threeAction').click(
            function () {
                cyberhawk.ajaxRequest("threeResult", false, "threes");
            }
        );

        $('.fiveAction').click(
            function () {
                cyberhawk.ajaxRequest("fiveResult", false, "fives");
            }
        );

        $('.fifteenAction').click(
            function () {
                cyberhawk.ajaxRequest("fifteenResult", false, "threesAndFives");
            }
        );

        $('.one').pagination(
            {
                dataSource: cyberhawk.backUrl + "?action=page",
                locator: "items",
                pageSize: 10,
                totalNumber:1000,
                showFirstOnEllipsisShow:false,
                callback: function (data, pagination) {
                    cyberhawk.mainSuccess(data, "pageResult", false);
                }
            }
        )

    }
);