<?php
header('Access-Control-Allow-Origin: *');
require "CyberhawkTest.php";

$test = new CyberhawkTest();

if (getAction() == "page") {
    $pageInfo = getPageInfo();
    $test->getPagingData($pageInfo);
    $test->formatPageData();
} elseif (getAction() == 'value') {
    $number = getNumber();
    $test->calculateOutput($number, $number);
} elseif (getAction() == 'fives') {
    $test->calculateOutput();
    $test->getFives();
} elseif (getAction() == 'threes') {
    $test->calculateOutput();
    $test->getThrees();
} elseif (getAction() == 'threesAndFives') {
    $test->calculateOutput();
    $test->getThreesAndFives();
} else {
    $test->calculateOutput();
}

/**
 * @return bool|string
 */
function getAction()
{
    if (empty($_GET['action'])) {
        return false;
    }
    return $_GET['action'];
}

/**
 * @return int
 */
function getNumber()
{
    if (empty($_GET['number'])) {
        return 0;
    }
    return $_GET['number'];
}

/**
 * @return array
 */
function getMinMax()
{
    $rangeArray = ["min" => 0, "max" => 0];
    if (!empty($_GET['min'])) {
        $rangeArray['min'] = $_GET['min'];
    }
    if (!empty($_GET['max'])) {
        $rangeArray['max'] = $_GET['max'];
    }
    return $rangeArray;
}

/**
 * @return array
 */
function getPageInfo()
{
    $pageInfo = ["size" => 1, "number" => 1];
    if (!empty($_GET['pageSize'])) {
        $pageInfo['size'] = $_GET['pageSize'];
    }
    if (!empty($_GET['pageNumber'])) {
        $pageInfo['number'] = $_GET['pageNumber'];
    }
    return $pageInfo;
}

echo json_encode($test->getOutput());
